#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PatrolRoute.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTINGGROUNDS_API UPatrolRoute : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(EditInstanceOnly, Category = Patrol)
	TArray<AActor*> PatrolPoints;

public:
	UFUNCTION()
	TArray<AActor*> GetPatrolPoints() const;
};
