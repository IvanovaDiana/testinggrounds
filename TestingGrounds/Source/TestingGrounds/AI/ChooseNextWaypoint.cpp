#include "ChooseNextWaypoint.h"

EBTNodeResult::Type UChooseNextWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	UBlackboardComponent* BlackBoardComp = OwnerComp.GetBlackboardComponent();
	UPatrolRoute* PatrolRoute = OwnerComp.GetAIOwner()->GetPawn()->FindComponentByClass<UPatrolRoute>();
	if (BlackBoardComp && PatrolRoute)
	{
		int32 IndexInt = BlackBoardComp->GetValueAsInt(Index.SelectedKeyName);
		if (PatrolRoute->GetPatrolPoints().Num() > IndexInt) // Check PatrolPoints is set up
		{
			BlackBoardComp->SetValueAsObject(Point.SelectedKeyName, PatrolRoute->GetPatrolPoints()[IndexInt]); // Set up way point
			BlackBoardComp->SetValueAsInt(Index.SelectedKeyName, (IndexInt + 1) % PatrolRoute->GetPatrolPoints().Num()); // Set up new index
			return EBTNodeResult::Succeeded;
		}
	}
	UE_LOG(LogTemp, Error, TEXT("Failed on task: ChooseNextWaypoint for %s"), *OwnerComp.GetAIOwner()->GetPawn()->GetName());
	return EBTNodeResult::Failed;
}