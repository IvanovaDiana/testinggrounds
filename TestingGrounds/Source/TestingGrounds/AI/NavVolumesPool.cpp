// Fill out your copyright notice in the Description page of Project Settings.

#include "NavVolumesPool.h"
#include "NavMesh/NavMeshBoundsVolume.h"

UNavVolumesPool::UNavVolumesPool()
{
}

ANavMeshBoundsVolume* UNavVolumesPool::Checkout()
{
	if (Pool.Num() > 0)
	{
		return Pool.Pop();
	}
	return nullptr;
}

void UNavVolumesPool::Add(ANavMeshBoundsVolume* Actor)
{
	if (Actor)
	{
		Pool.Add(Actor);
	}
}