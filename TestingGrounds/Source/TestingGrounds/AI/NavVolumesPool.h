// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "NavVolumesPool.generated.h"

class ANavMeshBoundsVolume;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTINGGROUNDS_API UNavVolumesPool : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<ANavMeshBoundsVolume*> Pool;
public:	
	// Sets default values for this component's properties
	UNavVolumesPool();

	// Checkout ANavMeshBoundsVolume from pool
	ANavMeshBoundsVolume* Checkout();

	// Add ANavMeshBoundsVolume to pool
	void Add(ANavMeshBoundsVolume* Actor);
};
