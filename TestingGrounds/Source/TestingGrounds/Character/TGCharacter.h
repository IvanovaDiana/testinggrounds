#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Weapon/Gun.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "TGCharacter.generated.h"

UCLASS()
class TESTINGGROUNDS_API ATGCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCamera;

	/** Gun blueprint */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	AGun* Gun;

protected:
	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;

	/** Fires a projectile. */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void PullTrigger();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

public:	
	/** Sets default values for this character's properties */
	ATGCharacter();

	/** Called to bind functionality to input */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Called when controller no longer possesses character */
	virtual void UnPossessed() override;

	/** Gun blueprint */
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<class AGun> GunBlueprint;

	/** Setup is character jumping or not for animation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
	bool JumpButtonDown;

	/** Setup is character aiming or not for animation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
	bool Aiming;

	/** Health point of character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
	float Health;

	/** Return is character out of health or not */
	UFUNCTION(BlueprintPure, Category = Input)
	bool IsDead() const;
};
