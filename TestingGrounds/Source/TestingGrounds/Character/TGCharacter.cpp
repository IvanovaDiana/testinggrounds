#include "TGCharacter.h"

ATGCharacter::ATGCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// Create a CameraComponent	
	FirstPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCamera->SetupAttachment(GetCapsuleComponent());
	FirstPersonCamera->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCamera->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCamera);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);
}

void ATGCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GunBlueprint == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Gun blueprint missing."));
	}
	else
	{
		Gun = GetWorld()->SpawnActor<AGun>(GunBlueprint);

		//Attach gun mesh component to Skeleton, doing it here because the skelton is not yet created in the constructor
		if (IsPlayerControlled())
		{
			Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
			Gun->FPAnimInstance = Mesh1P->GetAnimInstance();
		}
		else
		{
			Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
			Gun->TPAnimInstance = GetMesh()->GetAnimInstance();
		}
	}
}

void ATGCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ATGCharacter::PullTrigger);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ATGCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATGCharacter::MoveRight);

	// Bind rotation events
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

void ATGCharacter::UnPossessed()
{
	Super::UnPossessed();

	if (Gun) { Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint")); }
}

void ATGCharacter::PullTrigger()
{
	if (Gun) { Gun->OnFire(); }
}

void ATGCharacter::MoveForward(float Value)
{
	if (Value != 0.0f) { AddMovementInput(GetActorForwardVector(), Value); }
}

void ATGCharacter::MoveRight(float Value)
{
	if (Value != 0.0f) { AddMovementInput(GetActorRightVector(), Value); }
}

bool ATGCharacter::IsDead() const
{
	return (Health <= 0.f);
}