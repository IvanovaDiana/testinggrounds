// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "TestingGroundsGameMode.h"
#include "TestingGroundsHUD.h"
#include "UObject/ConstructorHelpers.h"

#include "NavMesh/NavMeshBoundsVolume.h"
#include "EngineUtils.h"
#include "AI/NavVolumesPool.h"

ATestingGroundsGameMode::ATestingGroundsGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Characters/Character/BP_TGCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATestingGroundsHUD::StaticClass();

	NavVolumesPool = CreateDefaultSubobject<UNavVolumesPool>(FName("Nav Volume Pool"));
}

void ATestingGroundsGameMode::AddToPool(ANavMeshBoundsVolume* VolumeToAdd)
{
	if(NavVolumesPool) NavVolumesPool->Add(VolumeToAdd);
}

void ATestingGroundsGameMode::PopulateBoundsVolumePool()
{
	auto VolumeIterator = TActorIterator<ANavMeshBoundsVolume>(GetWorld());
	while (VolumeIterator)
	{
		AddToPool(*VolumeIterator);
		++VolumeIterator;
	}
}