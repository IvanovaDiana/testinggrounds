// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestingGroundsGameMode.generated.h"

class UNavVolumesPool;
class ANavMeshBoundsVolume;

UCLASS(minimalapi)
class ATestingGroundsGameMode : public AGameModeBase
{
	GENERATED_BODY()

	void AddToPool(ANavMeshBoundsVolume* VolumeToAdd);

public:
	ATestingGroundsGameMode();

	// Get all ANavMeshBoundsVolume and save it to pool
	UFUNCTION(BlueprintCallable, Category = "Bounds Pool")
	void PopulateBoundsVolumePool();

	// Pool with ANavMeshBoundsVolume for possibility to move them
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Bounds Pool")
	UNavVolumesPool* NavVolumesPool;
};



