#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

class UNavVolumesPool;
class ANavMeshBoundsVolume;

UCLASS()
class TESTINGGROUNDS_API ATile : public AActor
{
	GENERATED_BODY()

	// Return true if sphere hit something by camera channel
	bool CastSphere(FVector Location, float Range);

	UNavVolumesPool* NavVolumesPool;
	ANavMeshBoundsVolume* NavVolume;

	UPROPERTY()
	TArray<AActor*> SpawnedPool;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called whenever this actor is being removed from a level
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	// Box for SpawnActors function
	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FBox BoxForSpawning;

	// Offset for nav mesh to move
	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FVector NavigationBoundsOffset;

public:	
	// Sets default values for this actor's properties
	ATile();

	// Spawn actors in random points (check collision in radius)
	UFUNCTION(BlueprintCallable, Category = "Procedural Generation")
	void SpawnActors(const TSubclassOf<AActor> ToSpawn, int32 MinSpawn, int32 MaxSpawn, float Radius = 500);

	// Destroy all spawned actors
	UFUNCTION(BlueprintCallable, Category = "Procedural Generation")
	void DestroyAllSpawned();

	// Spawn ai pawns in random points (check collision in radius)
	UFUNCTION(BlueprintCallable, Category = "Procedural Generation")
	void SpawnAIPawns(const TSubclassOf<APawn> ToSpawn, int32 MinSpawn, int32 MaxSpawn, float Radius = 100);

	// Get a reference to a UNavVolumesPool and get ANavMeshBoundsVolume from it
	UFUNCTION(BlueprintCallable, Category = "Bounds Pool")
	void SetPool(UNavVolumesPool* Pool);
};
