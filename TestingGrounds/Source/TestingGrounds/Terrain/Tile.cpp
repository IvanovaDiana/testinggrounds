#include "Tile.h"
#include "AI/NavVolumesPool.h"
#include "NavMesh/NavMeshBoundsVolume.h"
#include "AI/NavigationSystemBase.h"

ATile::ATile()
{ }

void ATile::BeginPlay()
{
	Super::BeginPlay();
}

void ATile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	NavVolumesPool->Add(NavVolume);
}

void ATile::SpawnActors(const TSubclassOf<AActor> ToSpawn, int32 MinSpawn, int32 MaxSpawn, float Radius)
{
	if (ToSpawn && MaxSpawn >= MinSpawn && MaxSpawn)
	{
		const int32 MAX_ATTEMPTS = 100;
		int32 Range = FMath::RandRange(MinSpawn, MaxSpawn);
		for (int32 i = 0; i < Range; i++)
		{
			FVector SpawnPoint;
			float RandomScale = FMath::RandRange(0.6f, 2.f);
			for (int32 i = 0; i < MAX_ATTEMPTS; i++)
			{
				SpawnPoint = FMath::RandPointInBox(BoxForSpawning);
				if (!CastSphere(ActorToWorld().TransformPosition(SpawnPoint), Radius * RandomScale))
				{
					AActor* Spawned = GetWorld()->SpawnActor(ToSpawn);
					if (Spawned)
					{
						Spawned->SetActorLocation(SpawnPoint);
						Spawned->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
						Spawned->SetActorRotation(FRotator(0.f, FMath::RandRange(-180.f, 180.f), 0.f));
						Spawned->SetActorScale3D(FVector(RandomScale));

						SpawnedPool.Add(Spawned);
					}
					break;
				}
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Incorrect argument for SpawnActors"));
	}
}

void ATile::SpawnAIPawns(const TSubclassOf<APawn> ToSpawn, int32 MinSpawn, int32 MaxSpawn, float Radius)
{
	if (ToSpawn && MaxSpawn >= MinSpawn && MaxSpawn)
	{
		const int32 MAX_ATTEMPTS = 100;
		int32 Range = FMath::RandRange(MinSpawn, MaxSpawn);
		for (int32 i = 0; i < Range; i++)
		{
			FVector SpawnPoint;
			for (int32 i = 0; i < MAX_ATTEMPTS; i++)
			{
				SpawnPoint = FMath::RandPointInBox(BoxForSpawning);
				if (!CastSphere(ActorToWorld().TransformPosition(SpawnPoint), Radius))
				{
					APawn* AIPawn = GetWorld()->SpawnActor<APawn>(ToSpawn);
					if (AIPawn)
					{
						AIPawn->SetActorLocation(SpawnPoint);
						AIPawn->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
						AIPawn->SetActorRotation(FRotator(0.f, FMath::RandRange(-180.f, 180.f), 0.f));
						AIPawn->SpawnDefaultController();
						AIPawn->Tags.Add(FName("Enemy"));

						SpawnedPool.Add(AIPawn);
					}
					break;
				}
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Incorrect argument for SpawnActors"));
	}
}

void ATile::DestroyAllSpawned()
{
	for (auto SpawnedActor : SpawnedPool)
	{
		TArray<AActor*> AttachedActors;
		SpawnedActor->GetAttachedActors(AttachedActors);
		for (auto AttachedActor : AttachedActors)
		{
			AttachedActor->Destroy();
		}
		SpawnedActor->Destroy();
	}
}

bool ATile::CastSphere(FVector Location, float Radius)
{
	FHitResult HitResult;
	return GetWorld()->SweepSingleByChannel(
		HitResult,
		Location,
		Location,
		FQuat::Identity,
		ECollisionChannel::ECC_GameTraceChannel2,
		FCollisionShape::MakeSphere(Radius)
	);
}

void ATile::SetPool(UNavVolumesPool* Pool)
{
	NavVolumesPool = Pool;

	NavVolume = NavVolumesPool->Checkout();
	if (NavVolume)
	{
		NavVolume->SetActorLocation(GetActorLocation() + NavigationBoundsOffset);
		FNavigationSystem::Build(*GetWorld());
	}
}